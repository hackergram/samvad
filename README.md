# samvad (समवाद)

Conversational Collaboration for Communities

## What it is
Community Engagement, Monitoring and Evaluation Framework for Social Enterprises

## Why it's needed
Enables social enterprises to connect with communities and individuals across interfaces, which is currently not possible with the existing social media, which is corporation oriented. 

## Who is maintaining
[हैकरgram](http://hackergram.org) Members in Uttarakhand, India (listener at hackergram dot org)


[Graph Schema](https://gitlab.com/hackergram/samvad/uploads/7b898d7dade2dd8e85ef368705f28f2b/image.png)
