"""
Created on Sat Sep  8 21:52:07 2018s
@author: arjunvenkatraman
"""

import mongoengine
from samvad import documents, utils
# Setting up mongoengine connections
mongoengine.connect('samvad', alias='default')

# CRUD Functions for document.User
def create_user(user_id, mobile_num):
    user = documents.User(user_id=user_id, mobile_num=mobile_num)
    user.save()
    return user 

def get_user(user_id):
    user = documents.User.objects(user_id=user_id).first()
    return user

def update_user(user_id, mobile_num):
    user = documents.User.objects(user_id=user_id).first()
    user.mobile_num = mobile_num
    user.save()
    return user

def delete_user(user_id):
    user = documents.User.objects(user_id=user_id).first()
    user.delete()
    return user
